//userscript header
// ==UserScript==
// @name           piss.js
// @namespace      http://www.piss.com
// @description    screw with cnap.hv.se getapod Lulw
// @include        *://cnap.hv.se/getapod/show.php*
// ==/UserScript==

var piss_username = "remi";

document.querySelectorAll("table")[3].querySelectorAll("br").forEach(br => {
    br.style.display = "none";
});

document.querySelectorAll("table")[3].querySelectorAll("a").forEach(a => {
    if (a.href.split("http://cnap.hv.se/getapod/")[1].split(".php")[0] == "bookForm") {
        a.onclick = e => {
            e.preventDefault();
            //fetch post to book.php
            var form = new FormData();
            //url split
            var url = URLToArray(a.href);
            //console.log(url);
            form.append("room", url.room);
            form.append("time", url.time);
            form.append("pod", url.pod);
            form.append("name1", piss_username);
            //get random password
            var password = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            form.append("pass1", password);
            form.append("name2", "");
            form.append("name3", "");
            form.append("comment", "");
            const queryString = new URLSearchParams(form).toString()
            fetch("http://cnap.hv.se/getapod/book.php", {
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                body: queryString
            }).then(response => {
                location.reload();
            });
        }
    } else {
        if (a.innerHTML == piss_username) {
            a.style.color = "green";
        } else {
            a.style.color = "red";
        }
        //delete booking
        a.onclick = e => {
            e.preventDefault();
            var form = new FormData();
            var url = URLToArray(a.href);
            form.append("room", url.room);
            form.append("time", url.time);
            form.append("pod", url.pod);
            form.append("pass1", url.pass1);
            const queryString = new URLSearchParams(form).toString()
            fetch("http://cnap.hv.se/getapod/delete.php", {
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                body: queryString
            }).then(response => {
                location.reload();
            });
        }
    }
});

function URLToArray(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        if (!pairs[i])
            continue;
        var pair = pairs[i].split('=');
        request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
    }
    return request;
}